<!-- TITLE: DoD Drone Knowledgebase -->
<!-- SUBTITLE: A catalog of commercial UAS capabilities, vulnerabilities, and mitigations -->

This website is intended to help commanders across the Department of Defense make informed decisions about benefits, risks, and mitigation measures for common small UAS (SUAS) platforms.

Each drone is coded with a Technical Vulnerability score: SECURE, PARTIALLY SECURE, or INSECURE. Troops considering using a SUAS must weigh this Technical Vulnerability score against their assessment of the threat. How likely is it that an adversary will attempt to exploit the drone, and how serious are the consequences if the adversary succeeds? 

The chart below compares Technical Vulnerability versus Threat to yield a risk assessment.


<p style="text-align:center;"><img src="/uploads/fig-drone-vulnerability.png" alt="drawing" style="width: 500px;"/></p>

<!--![Fig Drone Vulnerability](/uploads/fig-drone-vulnerability.png "Fig Drone Vulnerability") -->

<table id="center">
<tbody>
<tr>
<td colspan="2"><strong>Guidance for accepting risk at various levels:</strong></td>
</tr>
<tr>
<td width="25%"><font color="green">GREEN</font></td>
  <td width="75%">SUAS usage poses little risk and is nearly always acceptable.</td>
</tr>
<tr>
<td width="25%"><font color="black">YELLOW</font></td>
  <td width="75%">Mission importance warrants accepting limited exploitation risks.</td>
</tr>
<tr>
<td width="25%"><font color="red">RED</font></td>
  <td width="75%">SUAS use is essential to complete mission or to protect human life.</td>
</tr>
</tbody>
</table>

## Vulnerability Catalog
* [Data leakage to foreign servers](/vuln-data-leakage)
* [Registration using PII](/vuln-registration)
* [Backdoor installation of software updates](/vuln-backdoor)
* [No fly zone / altitude restrictions](/vuln-nfz)
* [Storage of imagery/video on drone](/vuln-media)
* [Storage of historical flight logs on drone](/vuln-media)
* [Video interception](/vuln-video-intercept)
* [Datalink interception](/vuln-datalink-intercept)
* [Datalink injection/hijacking](/vuln-datalink-inject)
## Drone Manufacturers
* [DJI](/dji-models)
* [InstantEye](/instant-eye-models)
* [Autel](/autel-modles)
* [DIY/Pixhawk](/pixhawk-models)
* [GoPro](/gopro-models)
* [Skydio](/skydio-models)
* [GDU](/gdu-models)
