<!-- TITLE: InstantEye Models -->


<p style="text-align:center;"><img src="/uploads/instant-eye.png" alt="drawing" style="width: 300px;"/></p>

# General Assessment
<table id="center">
<tbody>
<tr>
<td colspan="2"><strong>Technical Vulnerability levels for different configurations:</strong></td>
</tr>
<tr>
<td width="25%">INSECURE</td>
  <td width="75%">Mk-2 GEN 3 and Mk-2 GEN 4</td>
</tr>
<tr>
<td width="25%">UNKNOWN</td>
  <td width="75%">MK-2 GEN 5</td>
</tr>
</tbody>
</table>

The InstantEye is advertised as "mil-spec", but evaluations of the GEN 3 and GEN 4 have revealed that it is designed using hobby-grade Chinese parts that are up to 14 years old. The hobby-grade XBee radios are vulnerable to publicly-available exploits such as replay attacks that can inject commands; a DIUx team was able to break into the datalink after watching a single YouTube video. The analog video link also has no security; anyone can purchase off-the-shelf FPV glasses or a screen and watch the video feed.

No technology solutions exist to mitigate these vulnerabilities.
# Required Mitigations
<ol>
<li>Train users on known vulnerabilities</li>
</ol>


# Models
## MK-2
* Mk-2 GEN3
* Mk-2 GEN4
* Mk-2 GEN5