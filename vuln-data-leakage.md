<!-- TITLE: Data Leakage to Foreign Servers -->

# Description

Any hardware device that is connected to an IP-based network can transmit data packets to destination IP addresses. Such devices could include drones, RC transmitters, mobile phones/tablets, ground control station computers, or even certain drone payloads. Packets could flow over Wi Fi, LTE, wired connections, satellite Internet, or even mesh radio networks.

In our world of mobile phones, many apps continually gather data and transmit it to remote servers for processing. Examples include Google Maps fetching street maps imagery of your location, Yelp tracking your location, or Apple's Siri transmitting a recording of your voice for natural language processing. We generally trust servers located in the U.S. that are subject to strong physical security, software security, and legal protections.

Data leakage poses a problem if we believe adversaries have access to those servers and are mining data for use against us. 

Examples of ways drones could leak data:
* Drones or ground stations sending telemetry such as latitude and longitude
* Ground stations fetching imagery tiles from map servers, which give away position


# Potential Impact
If an adversary was collecting live telemetry of UAS telemetry, it could have the following impact:
* Give away position of U.S. or allied troops employing drones
* Reveal techniques, tactics, and procedures
* Provide details about battle rhythm based on dates and times of flights
# Mitigation strategies

Below are general strategies for mitigating the risk of data leakage:
* Obtain the source code for all components and inspect all outgoing URLs or IP addresses. This reveals exactly where traffic is flowing. Note that some URLs or IP addresses may be encoded in binary libraries.
* Use a tool like [Wireshark](wireshark.org) to inspect all outgoing traffic in a controlled environment.
* On Android, remove all Internet permissions from the app. This may require decompiling the app and editing the application's manifest. This is a sledgehammer approach that will block all Internet functionality.
* Pass all outgoing traffic through a proxy, which can be implemented either in software or hardware. Proxies can log all outgoing IP addresses, and filter using whitelists or blacklists.
