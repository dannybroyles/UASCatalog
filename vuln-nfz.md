<!-- TITLE: No Fly Zones and Altitude Restrictions -->

# Description
Vendors may impose no-fly zones or altitude restrictions on their drones. These are often implemented for good reason: to prevent users from willingly or unwillingly violating unauthorized airspace and posing a risk to others.

However, implementing no fly zones is problematic because many users have a justified need--and authorization--to operate in these zones. The imposition of NFZs can prevent legitimate users from flying authorized missions.

NFZs are especially problematic if the vendor can modify them through over-the-air updates. A drone owner might find herself unable to operate one day because NFZ implementation has changed. If an adversary controls the updating process, this could be one attack vector for grounding U.S. or allied drones.
# Potential Impact
* Impaired ability to fly drones near airports or special use airspace
* Limitation to a ceiling (often 400ft AGL to comply with FAA regulations)
* Denial of access to certain regions through over-the-air updates
# Mitigation
* Software solutions exist for disabling NFZs on some drones
* It is possible to obscure the GPS on some drones, preventing the drone from knowing its position. This can bypass NFZ restrictions, but also limits the drone's utility and makes it more difficult to control.