<!-- TITLE: Registration Using Personally Identifying Information (PII) -->

# Description

Some drones may require a one-time activation of hardware or software by registering on the vendor's website. This often requires furnishing personally identifying information like name, address, phone number, or e-mail address. The vendor may link this PII to the drone serial number.

# Potential Impact
Provision of PII could have the following impact:
* Drones could be permanently attributable to the owner
* Vendors could filter key terms such as ".mil" e-mail addresses to build a roster of drones belonging to government users
# Mitigation

* Register the drone using false credentials and a burner e-mail address
* Purchase the drone from a third-party that performs activation using their own credentials
* Spoof the activation process using a man-in-the-middle attack
* Patch out the activation process in the drone or GCS software