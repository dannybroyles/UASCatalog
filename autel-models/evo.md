<!-- TITLE: Autel Evo -->
<!-- SUBTITLE: A quick summary of the Autel Evo -->
  <img src="/uploads/autelevo.jpg" width="30%">

# General Assessment
The Autel Evo is an impressive clone of the DJI Mavic Pro. It is only slighter larger, has a similar form factor, and has a better camera. Autel has built its drone business around cloning DJI products. Its X-Star (a clone of the Phantom series) did not perform well on the market and is longer supported. However, the Evo has created significant Buzz because there is so much appetite to move away from the DJI. Autel is a Chinese company with an American arm, but has deliberately marketed itself as a "secure" alternative to DJI.

DIUx obtained a pre-release unit and found the flight characteristics to be impressive. We feel comfortable recommending this drone to troops that need an organic ISR platform. DIUx has only done a preliminary vulnerability analysis. We decompiled and inspected the Autel GCS app for Android and did not find evidence of data leakage. The app contains two map services: (1) Google Maps and (2) AMaps, a Chinese mapping engine. AMaps does call back to Chinese servers for map tiles when activated, but AMaps is only activated if Google Maps is unavailable. Autel likely made this design choice because Google Maps is not allowed in China. DIUx also inspected outgoing traffic with Wireshark and did not see any sign of data flowing to foreign servers.

# Risks and Mitigation
No evidence of data leakage. Other risk factors TBD.
# RF Data

[Link to FCC approval](https://fccid.io/2AGNTEVORC582409A)

# Specifications
[Specs from Autel's website](https://www.autelrobotics.com/evo/)


<p><span>
</span></p><table id="tablepress-20" class="tablepress tablepress-id-20 table-tech-specs">
<tbody>
<tr class="row-1">
	<td colspan="2" class="column-1"><span class="th">Aircraft</span></td>
</tr>
<tr class="row-2">
	<td class="column-1"><span>Hover Precision</span></td><td class="column-2">GPS+ Ultrasonic+IMU: Horizontal: ±1.5m, Vertical: ±0.2m;<br>
Vision+IMU: Horizontal: ±0.1m, Vertical: ±0.1m</td>
</tr>
<tr class="row-3">
	<td class="column-1"><span>Max. Yaw Rate</span></td><td class="column-2">200 dps</td>
</tr>
<tr class="row-4">
	<td class="column-1"><span>Max. Inclination Angle</span></td><td class="column-2">35° </td>
</tr>
<tr class="row-5">
	<td class="column-1"><span>Max. Ascent/Descent Speed</span></td><td class="column-2">Ascent: 5m/s Descent: 3m/s</td>
</tr>
<tr class="row-6">
	<td class="column-1"><span>Max. Horizontal Speed</span></td><td class="column-2">20 m/s (44.7mph)</td>
</tr>
<tr class="row-7">
	<td class="column-1"><span>Diagonal Wheelbase</span></td><td class="column-2">338 mm</td>
</tr>
<tr class="row-8">
	<td class="column-1"><span>Propeller Size</span></td><td class="column-2">8.3" x 2.9"</td>
</tr>
<tr class="row-9">
	<td class="column-1"><span>Video Link Frequency</span></td><td class="column-2">2.4GHz~2.4835GHz 902MHz~928MHz</td>
</tr>
<tr class="row-10">
	<td class="column-1"><span>Receiver Frequency</span></td><td class="column-2">2.4GHz~2.4835GHz 902MHz~928MHz</td>
</tr>
<tr class="row-11">
	<td class="column-1"><span>Flight Modes</span></td><td class="column-2">GPS · ATTI · SPORT<br>
</td>
</tr>
<tr class="row-12">
	<td class="column-1"><span>Operating Environment Temperature</span></td><td class="column-2">32° F ~ 104° F (0° C ~ 40° C)</td>
</tr>
<tr class="row-13">
	<td class="column-1"><span>Storage Temperature</span></td><td class="column-2">-10°C~40°C (14°F~104°F)</td>
</tr>
<tr class="row-14">
	<td class="column-1"><span>Weight (Battery &amp; Propellers included)</span></td><td class="column-2">863g (1.90lbs)</td>
</tr>
<tr class="row-15">
	<td class="column-1"></td><td class="column-2"></td>
</tr>
<tr class="row-16">
	<td colspan="2" class="column-1"><span class="th">Camera</span></td>
</tr>
<tr class="row-17">
	<td class="column-1"><span>Operating Environment Temp.</span></td><td class="column-2">0°C~40°C (32°F~104°F)</td>
</tr>
<tr class="row-18">
	<td class="column-1"><span>Still Photography Modes</span></td><td class="column-2">Single shot<br>
Burst shooting<br>
Auto Exposure Bracketing (AEB) Time-lapse</td>
</tr>
<tr class="row-19">
	<td class="column-1"><span>Video Recording Modes</span></td><td class="column-2">Normal · Picture In Video (PIV)</td>
</tr>
<tr class="row-20">
	<td class="column-1"><span>Max Frame Rates</span></td><td class="column-2">4K60, 2.7K60, 1080p120, 720p240</td>
</tr>
<tr class="row-21">
	<td class="column-1"><span>Max Field of View</span></td><td class="column-2">94°</td>
</tr>
<tr class="row-22">
	<td class="column-1"><span>Supported SD Card Types</span></td><td class="column-2">Micro-SD, 4 GB - 128 GB, Class 10 or UHS-3</td>
</tr>
<tr class="row-23">
	<td class="column-1"><span>File Formats</span></td><td class="column-2">FAT32/exFAT, Photo: JPG/DNG/JPG+DNG, Video: MOV/MP4 H.264 or H.265</td>
</tr>
<tr class="row-24">
	<td colspan="2" class="column-1"><span class="th">Aircraft Battery</span></td>
</tr>
<tr class="row-25">
	<td class="column-1"><span>Battery Type</span></td><td class="column-2">Rechargeable Lithium-Polymer Battery</td>
</tr>
<tr class="row-26">
	<td class="column-1"><span>Capacity</span></td><td class="column-2">4300 mAh</td>
</tr>
<tr class="row-27">
	<td class="column-1"><span>Battery Voltage Charging</span></td><td class="column-2">11.4 V</td>
</tr>
<tr class="row-28">
	<td class="column-1"><span>Charging Environment Temperature</span></td><td class="column-2">10°C~45°C (50°F~113°F)</td>
</tr>
<tr class="row-29">
	<td class="column-1"><span>Discharging Environment Temperature</span></td><td class="column-2">-20°C~60°C (-4°F~140°F)</td>
</tr>
<tr class="row-30">
	<td class="column-1"><span>Storage Temperature &amp; Humidity</span></td><td class="column-2">Temp: -10°C~40°C (14°F~104°F) Humidity: 65±20%RH</td>
</tr>
<tr class="row-31">
	<td class="column-1"><span>Flight Time</span></td><td class="column-2">Up to 30 minutes</td>
</tr>
<tr class="row-32">
	<td colspan="2" class="column-1"><span class="th">Remote Controller</span></td>
</tr>
<tr class="row-33">
	<td class="column-1"><span> OLED Screen Brightness</span></td><td class="column-2">330 nits</td>
</tr>
<tr class="row-34">
	<td class="column-1"><span> Max Operating Time</span></td><td class="column-2">3 hours </td>
</tr>
<tr class="row-35">
	<td class="column-1"><span>RF Receiver Operating Frequency</span></td><td class="column-2">2.4GHz~2.4835GHz 902MHz~928MHz</td>
</tr>
<tr class="row-36">
	<td class="column-1"><span>Video Link Frequency</span></td><td class="column-2">2.4GHz~2.4835GHz<br>
902MHz~928MHz</td>
</tr>
<tr class="row-37">
	<td class="column-1"><span>Operating Temperature</span></td><td class="column-2">0°C~40°C (32°F~104°F)</td>
</tr>
<tr class="row-38">
	<td class="column-1"><span>Storage Temperature</span></td><td class="column-2">1 year: -20°C~25°C(-4°F~77°F)<br>
3 months: -20°C~45°C(-4°F~113°F)</td>
</tr>
<tr class="row-39">
	<td class="column-1"><span>Max Control and Video Transmission Distance</span></td><td class="column-2">7km (4.3miles)</td>
</tr>
<tr class="row-40">
	<td class="column-1"><span>Transmission Power (EIRP)</span></td><td class="column-2">FCC: &lt;=26 dBm CE: &lt;=20 dBm</td>
</tr>
<tr class="row-41">
	<td class="column-1"><span>Operating Current/Voltage</span></td><td class="column-2">2A @ 3.6V</td>
</tr>
<tr class="row-42">
	<td class="column-1"><span>Battery</span></td><td class="column-2">6700mAH</td>
</tr>
<tr class="row-43">
	<td class="column-1"><span>Power Consumption</span></td><td class="column-2">7.2W</td>
</tr>
<tr class="row-44">
	<td class="column-1"><span>Weight (battery included)</span></td><td class="column-2">370g (0.81lbs)</td>
</tr>
</tbody>
</table>
<!-- #tablepress-20 from cache --><p></p>
