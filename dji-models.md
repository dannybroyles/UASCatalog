<!-- TITLE: DJI Models -->

# Overall Assessment

<table id="center">
<tbody>
<tr>
<td colspan="2"><strong>Technical Vulnerability levels for different DJI configurations:</strong></td>
</tr>
<tr>
<td width="25%">INSECURE</td>
  <td width="75%">Stock firmware using any publicly available control app (DJI GO, Litchi, etc.)</td>
</tr>
<tr>
<td width="25%">PARTIALLY SECURE</td>
  <td width="75%">Stock firmware using Offline Drone Activation System (ODAS) or DARPA Scanner</td>
</tr>
<tr>
<td width="25%">SECURE</td>
  <td width="75%">Rizer firmware and Rizer control app (RS GO, Rosetta SECURE, or ATAK-GO with RS-VPN)</td>
</tr>
</tbody>
</table>

DoD recommends the use of RIZER to cyber-harden DJI drones, which can be downloaded from the [DIUx Rogue Squadron website](https://rogue.diux.mil). 

If troops must use DJI drones without RIZER, they should follow this procedural guidance to ensure the ground control station does not access the Internet: TODO.

# Required Mitigation

<ol>
  <li>Do not register the drone with DJI using real personal identifying information</li>
  <li>Ensure the ground control station does not reach the Internet</li>
  <li>If possible, install RIZER and use only RIZER-approved ground statio apps (RS-GO, Rosetta SECURE, or ATAK-GO with RS-VPN)</li>
</ol>


# Technical Analysis

Here are some known issues that are common to all DJI drones:

<table id="tablepress-20" class="tablepress tablepress-id-20 table-tech-specs">
<tbody>
<tr class="row-1">
	<td colspan="2" class="column-1"><span class="th"><a href=/vuln-data-leakage>Data leakage to foreign servers</a></span></td>
</tr>
<tr class="row-2">
<td class="column-1" width="50%">Multiple DoD reports have noted large amounts of encrypted data flowing to foreign servers.</td><td class="column-2" width="50%">
<ul>
<li>Rizer: </li> - Provides a patched DJI GO app with Internet permissions disabled. Also provides Rosetta SECURE and RS-VPN, which firewall all traffic in DJI apps unless traffic is specifically whitelisted.
<li>ODAS: </li> - Uses a Raspberry Pi device to "man-in-the-middle" the traffic stream, filtering unwanted traffic. 
<li>Procedural: </li> - Drones can be flown with the phone in Airplane mode. Apps should be deleted before reconnecting the phone to the Internet.
</td>
</tr>
</tbody>
</table>

<table id="tablepress-20" class="tablepress tablepress-id-20 table-tech-specs">
<tbody>
<tr class="row-1">
	<td colspan="2" class="column-1"><span class="th"><a href=/vuln-backdoor>Backdoor installation of software updates</a></span></td>
</tr>
<tr class="row-2">
<td class="column-1" width="50%"> In summer 2017, security analysts discovered that the DJI GO app included a hot-patching capability capability that allowed over-the-air updates without the user's knowledge. This violated the terms of service for both the Apple Store and Google Play Store. DJI claimed that this was a development tool that was included in the release build by mistake.</td><td class="column-2" width="50%">Offending source code was removed in summer 2017. Vulnerability no longer active.
</tr>
</tbody>
</table>

<table id="tablepress-20" class="tablepress tablepress-id-20 table-tech-specs">
<tbody>
<tr class="row-1">
	<td colspan="2" class="column-1"><span class="th"><a href=/vuln-nfz>No fly zone / altitude restrictions</a></span></td>
</tr>
<tr class="row-2">
<td class="column-1" width="50%">DJI imposes no fly zones around airports and in certain regions of the world, to include Iraq and Syria. DJI has a process for contacting DJI to unlock NFZs, but this requires furnishing details about where a drone with a given serial number will be operated.</td><td class="column-2" width="50%">Rizer's Drone Manager tool disables all no fly zones and altitude restrictions on the drone.
</tr>
</tbody>
</table>

<table id="tablepress-20" class="tablepress tablepress-id-20 table-tech-specs">
<tbody>
<tr class="row-1">
	<td colspan="2" class="column-1"><span class="th"><a href=/vuln-media>Storage of imagery/video on drone</a></span></td>
</tr>
<tr class="row-2">
<td class="column-1" width="50%">Imagery and video captured by the drone are stored on an onboard SD card.</td><td class="column-2" width="50%">The best mitigation is procedural. Before shooting imagery or video, troops should weigh the risks if they lose the drone and the media is retrieved by another party.

If troops do not need to recover high-definition imagery/video from the drone, they may also consider removing the SD card from the drone before flight.
</tr>
</tbody>
</table>

<table id="tablepress-20" class="tablepress tablepress-id-20 table-tech-specs">
<tbody>
<tr class="row-1">
	<td colspan="2" class="column-1"><a href=/vuln-logs>Storage of historical flight logs on drone</a></td>
</tr>
<tr class="row-2">
<td class="column-1" width="50%">Flight logs are stored on board the drone, either in an internally mounted SD card (not designed ot be removed) or a microcontroller. </td>
<td class="column-2" width="50%">Rizer includes a "blackbox" feature that automatically deletes flight logs as they are recorded.</td>
</tr>
</tbody>
</table>




# Models
## Inspire
* [Inspire 1](dji-models/inspire-1)
* Inspire 1 Pro/Raw
* Inspire 2
## Mavic
* Mavic Air
* [Mavic Pro](dji-models/mavic-pro)
* Mavic Pro Platinum
## Phantom 
* Phantom FC40
* Phantom 1
* Phantom 2
* Phantom 2 Vision
* Phantom 2 Vision+
* Phantom 3 Standard
* Phantom 3 Pro
* Phantom 3 Advanced
* Phantom 3 SE
* Phantom 3 4K
* Phantom 4
* [Phantom 4 Pro](dji-models/p4p)
* Phantom 4 pro v2.0
* Phantom 4 Advanced
## Spark
* Spark