<!-- TITLE: Backdoor Installation of Software Updates -->

# Description
In some cases, adversaries may be able to perform over-the-air updates of software without the user's knowledge.

Over-the-air updates may be perfectly benign, as they are used to continuously deliver updates and bug fixes and improve the quality of software. However, users should have the ability to opt out of live updates, and they should be notified before updates begin. Users should also trust the source of updates.

The Apple Store and Google Play both have policies requiring app developers to go through approved processes for pushing updates. Backdoor updating of apps is not permitted.
# Potential Impact
If an untrusted source pushes updates, the update could potentially include malicious code. Even routine updates might limit a drone's functionality, such as DJI retroactively imposing no-fly zones on users.

# Mitigation

* Only install trusted software
* Go through third-party software providers like Google Play or the Apple Store rather than sideloading binaries from questionable sources
* If users wish to use trusted versions of apps, disable updates and consider disabling Internet permissions