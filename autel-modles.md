<!-- TITLE: Autel Models -->

<tr><p style="text-align:center;"><img src="/uploads/autelevo.jpg" alt="drawing" style="width: 200px;"/></p></tr>


# Overall Assessment

<table id="center">
<tbody>
<tr>
<td colspan="2"><strong>Technical Vulnerability levels for different configurations:</strong></td>
</tr>
<tr>
<td width="25%">PARTIALLY SECURE</td>
  <td width="75%">Stock configuration using stock app</td>
</tr>
</tbody>
</table>

Autel is deliberately marketing the Evo as a "safe" alternative to the DJI Mavic Pro that does not violate user privacy. DIUx obtained a pre-release unit and conducted a preliminary software analysis by decompiling and inspecting Autel's app. The GCS app appears secure, with only benign callbacks to services like Google Maps for fetching satellite imagery. The app also contains callbacks to AMaps, a Chinese mapping service, but AMaps is only activated if Google Maps is unavailable. There is a rational business case for this, because Google Maps is banned in China.

While the drone and app appear to be safe, Autel is a Chinese company with an American subsidiary, much like DJI. Caution is warranted. Because the app contains all the code necessary to fetch data from AMaps, a small change to the app could result in the drone's position being broadcast to Chinese servers. Although Autel seems determined to stay above-board with its handling of data, DoD recommends taking precautions to keep the app from reaching out to the Internet.

# Required Mitigation

None required, but DoD recommends keeping the app from reaching the Internet through procedural measures.

# Models
* [EVO](autel-models/evo)
* X-STAR Premium
* Kestrel