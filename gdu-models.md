<!-- TITLE: GDU Models -->

<tr><p style="text-align:center;"><img src="/uploads/gdu-02.png" alt="drawing" style="width: 200px;"/></p></tr>

# Overall Assessment

<table id="center">
<tbody>
<tr>
<td colspan="2"><strong>Technical Vulnerability levels for different configurations:</strong></td>
</tr>
<tr>
<td width="25%">INSECURE</td>
  <td width="75%">Stock configuration using stock app</td>
</tr>
</tbody>
</table>

The GDU O2 appeared unexpectedly at Interdrone in fall 2017, from a Chinese company nobody had ever heard of. It held great promise as an alternative to the DJI Mavic Pro. The units began shipping in May. DIUx obtained two units, and quickly ruled them out.

The GDU is far inferior to the Mavic Pro. The first time DIUx charged batteries, the charger began smoking. DIUx attempted to decompile the app, but quickly determined that the code was messy and contained Chinese libraries. DIUx did not proceed any further, but recommends treating the drone as insecure.

# Required Mitigation

<ol>
<li>App should not be allowed to touch the Internet, by using prodedural measures.</li>
</ol>


# Models
## 02
* 02
* 02 Plus

## Byrd
* Standard
* Advanced
* Premium 1.0
* Premium 2.0