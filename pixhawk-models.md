<!-- TITLE: DIY/Pixhawk Models-->
<!-- SUBTITLE: Includes X-8 Skywalker, X-UAV Talon, SkyHunter, F450 quads, and many others -->

<table id="center">
<tbody>
<tr>
<td><p style="text-align:center;"><img src="/uploads/x-8.png" alt="drawing" style="width: 300px;"/></p></td>
<td><p style="text-align:center;"><img src="/uploads/talon.png" alt="drawing" style="width: 300px;"/></p></td>
</tr>
</tbody>
</table>

# Overall Assessment

<table id="center">
<tbody>
<tr>
<td colspan="2"><strong>Technical Vulnerability levels for different configurations:</strong></td>
</tr>
<tr>
<td width="25%">INSECURE</td>
  <td width="75%">Pixhawk-family drones using Sik/3DR radios or clones, X-Bee radios, or RFD900-family radios</td>
</tr>
<tr>
<td width="25%">PARTIALLY SECURE</td>
  <td width="75%">Pixhawk-family drones using WiFi or another type of encrypted datalink, but without Mavlink 2.0 protocol signing. Also Pixhawk drones using analog video.</td>
</tr>
<tr>
<td width="25%">SECURE</td>
  <td width="75%">Pixhawk-family drones using WiFi or another type of encrypted datalink, as well as Mavlink 2.0 with packet signing enabled.</td>
</tr>
</tbody>
</table>

Pixhawk-family systems use open-source software running on widely available autopilots. The software stack is usually [ArduPilot](http://ardupilot.org) or [PX4](http://px4.io). Because the software is open-source, anyone can inspect the code and ensure it contains nothing nefarious. 

The biggest vulnerability in these systems is the radios. The most widely-used radio for these systems is the SiK radio, once sold by 3DR and now sold by many other manufacturers. The radios contain no security, so anyone can snoop on datalinks and inject commands using simple, publicly-available hacks. Some older systems may still use X-Bee radios, which also have known security vulnerabilities.

Some DIY drones use WiFi, which has the same strengths and weaknesses as home or office WiFi. Always change default passwords and use strong passwords.

The Mavlink 1.0 protocol used by Pixhawk-family systems has no built-in security, which means anyone intercepting the datalink can inject packets. DoD users of these systems should switch to Mavlink 2.0 and enable packet signing, which means that the drone will only respond to packets from a trusted ground station.

# Required Mitigations
<ol>
<li>For WiFi-based systems, change default password</li>
<li>Enable Mavlink 2.0 and packet signing, if available</li>
<li>Only download firmwares from trusted sources</li>
</ol>

# Common Characteristics
Here are some known issues that are common to all drones equipped with Pixhawk autopilots:

# Models
* [Pixhawk](pixhawk-models/pixhawk)
* [Pixhawk 2](pixhawk-models/pixhawk2)
